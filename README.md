# `igwn/archive`

This project defines the `igwn/archive` docker image, which is just `igwn/base` with the `igwn-archive` package pre-installed.
