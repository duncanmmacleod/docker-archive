FROM igwn/base:el9

LABEL name="IGWN Source Distribution Archive"
LABEL maintainer="Duncan Macleod <duncan.macleod@ligo.org>"
LABEL support="Best Effort"

# install available updates
RUN dnf -y update && dnf clean all

# install igwn-archive
RUN dnf -y install igwn-archive \
    && dnf clean all
